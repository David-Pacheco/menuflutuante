unit UfrmPrincipal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Layouts, FMX.Effects, FMX.Ani, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.ListBox;

type
  TfrmPrincipal = class(TForm)
    Circle1: TCircle;
    ShadowEffect1: TShadowEffect;
    Layout1: TLayout;
    Rectangle1: TRectangle;
    FloatAnimation1: TFloatAnimation;
    Circle2: TCircle;
    ShadowEffect2: TShadowEffect;
    Circle3: TCircle;
    ShadowEffect3: TShadowEffect;
    Circle4: TCircle;
    ShadowEffect4: TShadowEffect;
    ListBox1: TListBox;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    ListBoxItem4: TListBoxItem;
    procedure FormCreate(Sender: TObject);
    procedure Circle1Click(Sender: TObject);
    procedure FloatAnimation1Finish(Sender: TObject);
    procedure Rectangle1Click(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure ListBoxItem1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.fmx}

procedure TfrmPrincipal.Circle1Click(Sender: TObject);
begin
  Layout1.Position.Y := frmPrincipal.Height + 20;
  Layout1.Visible := true;
  FloatAnimation1.Inverse := false;
  FloatAnimation1.StartValue := frmPrincipal.Height + 20;
  FloatAnimation1.StopValue := 0;
  FloatAnimation1.Start;
end;

procedure TfrmPrincipal.FloatAnimation1Finish(Sender: TObject);
begin
  if FloatAnimation1.Inverse = true then
    Layout1.Visible := false;
end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
 Layout1.Visible := false;
end;

procedure TfrmPrincipal.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  FloatAnimation1.Inverse := true;
  FloatAnimation1.Start;
end;

procedure TfrmPrincipal.ListBoxItem1Click(Sender: TObject);
begin
//  Label1.Text := 'Item 1 Clicado';
end;

procedure TfrmPrincipal.Rectangle1Click(Sender: TObject);
begin
  FloatAnimation1.Inverse := true;
  FloatAnimation1.Start;
end;

end.
